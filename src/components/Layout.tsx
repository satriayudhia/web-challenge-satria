import { useContext, useState } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import HeaderSplash from "../assets/images/header-splash.png";
import FooterSplash from "../assets/images/footer-splash.png";
import Logo from "../assets/images/logo.png";
import { BsBoxes, BsBarChartLine, BsSearch } from "react-icons/bs";
import Dropdown from "react-bootstrap/Dropdown";
import { AppContext } from "../context/AppContext";
import { HiMenu } from "react-icons/hi";

const Layout = () => {
  const [state, dispatch] = useContext(AppContext);
  const [show, setShow] = useState(false);

  const navigate = useNavigate();

  const { profile } = state;

  const handleLogout = () => {
    dispatch({
      type: "LOGOUT",
    });

    navigate("/");
  };

  return (
    <div className="container-fluid p-0">
      <div className="d-flex">
        <div
          className={`backdrop ${show && "active"}`}
          onClick={() => setShow(false)}
        />
        <div className={`sidebar ${show && "active"} shadow p-4`}>
          <img src={Logo} alt="logo" className="header-logo mb-5" />
          <img
            src={HeaderSplash}
            alt="header splash"
            className="header-splash"
          />
          <img
            src={FooterSplash}
            alt="footer splash"
            className="footer-splash"
          />

          <div className="sidebar-menu active d-flex align-items-center gap-2">
            <BsBoxes size={18} />
            <div className="">Products</div>
          </div>
          <div className="sidebar-menu d-flex align-items-center gap-2">
            <BsBarChartLine size={18} />
            <div className="">Reporting</div>
          </div>
        </div>

        <div className="w-100">
          <div className="d-flex justify-content-between align-items-center p-4 border-bottom">
            <div className="position-relative d-flex align-items-center gap-3">
              <HiMenu
                size={24}
                className="navbar-expand"
                onClick={() => setShow(true)}
              />
              <input
                type="text"
                className="input-search"
                placeholder="Cari disini.."
              />
              <BsSearch size={18} className="search-logo" />
            </div>
            <div className="d-flex align-items-center gap-3">
              <div className="text-end navbar-user">
                <div className="color-black" style={{ lineHeight: "12px" }}>
                  {profile?.firstName} {profile?.lastName}
                </div>
                <small className="color-gray" style={{ lineHeight: "12px" }}>
                  {profile?.email}
                </small>
              </div>

              <Dropdown>
                <Dropdown.Toggle>
                  <img
                    src="https://robohash.org/autquiaut.png?size=50x50&set=set1"
                    alt="user"
                  />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item>User Account</Dropdown.Item>
                  <Dropdown.Item>Setting</Dropdown.Item>
                  <hr />
                  <Dropdown.Item onClick={handleLogout}>Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          <div className="bg-content p-4">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Layout;
