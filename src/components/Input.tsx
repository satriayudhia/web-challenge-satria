import { FC, useState } from "react";
import { FaEyeSlash, FaEye } from "react-icons/fa";

interface Props {
  label?: string;
  type?: string;
  placeholder?: string;
  disabled?: boolean;
  value: any;
  onChange: (e: any) => void;
  containerClassName?: string;
  labelClassName?: string;
  inputClassName?: string;
}

const Input: FC<Props> = ({
  label,
  type = "text",
  placeholder,
  disabled,
  containerClassName,
  labelClassName,
  inputClassName,
  value,
  onChange,
}) => {
  const [inputType, setInputType] = useState(type);

  return (
    <div className={`${containerClassName}`}>
      {label && (
        <label className={`color-black ${labelClassName}`}>{label}</label>
      )}

      <div className="mt-1 position-relative">
        <input
          type={inputType}
          placeholder={placeholder}
          className={`input-default ${inputClassName}`}
          value={value}
          onChange={onChange}
          disabled={disabled}
        />

        {type === "password" && (
          <>
            {inputType === "text" && (
              <FaEye
                size={20}
                className="right-icon cursor-pointer"
                onClick={() => setInputType("password")}
              />
            )}
            {inputType === "password" && (
              <FaEyeSlash
                size={20}
                className="right-icon cursor-pointer"
                onClick={() => setInputType("text")}
              />
            )}
          </>
        )}
      </div>
    </div>
  );
};

export default Input;
