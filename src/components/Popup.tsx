import { FC } from "react";
import Modal from "react-bootstrap/Modal";
import { BsExclamationTriangle } from "react-icons/bs";
import { MdClose } from "react-icons/md";

interface Props {
  show: boolean;
  handleClose: () => void;
  content?: string;
}

const Popup: FC<Props> = ({ show, handleClose, content }) => {
  return (
    <Modal centered show={show} onHide={handleClose}>
      <Modal.Body>
        <div className="d-flex justify-content-between align-items-center gap-3">
          <div className="d-flex align-items-center gap-2">
            <BsExclamationTriangle size={20} className="color-red" />
            <div className="color-red">{content}</div>
          </div>

          <MdClose
            size={24}
            className="cursor-pointer color-red"
            onClick={handleClose}
          />
        </div>
      </Modal.Body>
    </Modal>
  );
};

export default Popup;
