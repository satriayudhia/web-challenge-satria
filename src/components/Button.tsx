import { FC } from "react";
import Spinner from "react-bootstrap/Spinner";

interface Props {
  type?: "button" | "submit" | "reset";
  text: string;
  onClick: (e: any) => void;
  className?: string;
  disabled?: boolean;
  loading?: boolean;
}

const Button: FC<Props> = ({
  type = "button",
  text,
  onClick,
  className,
  disabled,
  loading,
}) => {
  return (
    <button
      type={type}
      className={`button-default ${className}`}
      onClick={onClick}
      disabled={disabled || loading}
    >
      {loading && <Spinner animation="border" size="sm" className="me-2" />}
      {text}
    </button>
  );
};

export default Button;
