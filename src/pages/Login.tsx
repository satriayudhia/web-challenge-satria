import { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import HeaderLogin from "../assets/images/header-login.png";
import Logo from "../assets/images/logo.png";
import Input from "../components/Input";
import Button from "../components/Button";
import Popup from "../components/Popup";
import { API } from "../configs/api";
import { AppContext } from "../context/AppContext";

const Login = () => {
  const [state, dispatch] = useContext(AppContext);
  const [form, setForm] = useState({
    userId: "",
    password: "",
  });
  const [modal, setModal] = useState({ isOpen: false, content: "" });
  const [loading, setLoading] = useState(false);

  const { userId, password } = form;

  const navigate = useNavigate();

  const handleSubmit = async (e: any) => {
    try {
      e.preventDefault();
      if (!userId || !password) {
        return setModal({
          isOpen: true,
          content: "User ID dan atau Password anda belum diisi.",
        });
      }

      setLoading(true);
      const payload = {
        username: userId,
        password: password,
      };
      const res = await API.post("/auth/login", payload);
      if (res.status === 200) {
        dispatch({
          type: "SET_PROFILE",
          payload: res.data,
        });
        navigate("/home");
      }

      setLoading(false);
    } catch (error) {
      setLoading(false);
      setModal({
        isOpen: true,
        content: "Username atau Password yang anda isi salah.",
      });
    }
  };

  return (
    <>
      <Popup
        show={modal.isOpen}
        handleClose={() => setModal({ ...modal, isOpen: false })}
        content={modal.content}
      />

      <div className="container-fluid bg-purple p-0">
        <div className="container w-100 min-vh-100 d-flex align-items-center justify-content-center p-0">
          <div className="card-default position-relative shadow p-4">
            <img
              src={HeaderLogin}
              alt="header login"
              className="header-login"
            />

            <div className="d-flex justify-content-center mt-4">
              <img src={Logo} alt="logo" className="logo" />
            </div>

            <form onSubmit={handleSubmit} className="my-5">
              <h1 className="fw-bold text-xl color-black mb-1">Login</h1>
              <div className="color-black">Please sign in to continue.</div>

              <Input
                type="text"
                label="User ID"
                value={userId}
                onChange={(e) => setForm({ ...form, userId: e.target.value })}
                placeholder="User ID"
                containerClassName="mt-4"
                inputClassName="w-100"
                disabled={loading}
              />

              <Input
                type="password"
                label="Password"
                value={password}
                onChange={(e) => setForm({ ...form, password: e.target.value })}
                placeholder="Password"
                containerClassName="mt-4"
                inputClassName="w-100"
                disabled={loading}
              />

              <div className="d-flex justify-content-end mt-4">
                <Button
                  text="LOGIN"
                  onClick={handleSubmit}
                  type="submit"
                  loading={loading}
                />
              </div>
            </form>

            <div className="color-gray text-center">
              Don't have an account?{" "}
              <span className="color-orange cursor-pointer">Sign Up</span>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
