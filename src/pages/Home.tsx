import { useEffect, useState } from "react";
import { API } from "../configs/api";
import Popup from "../components/Popup";
import Placeholder from "react-bootstrap/Placeholder";

const Home = () => {
  const [modal, setModal] = useState({ isOpen: false, content: "" });
  const [products, setProducts] = useState<any>(null);
  const [total, setTotal] = useState(0);

  const getProducts = async () => {
    try {
      const res = await API.get("/products");
      setProducts(res.data.products);
      setTotal(res.data.total);
    } catch (error) {}
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <>
      <Popup
        show={modal.isOpen}
        handleClose={() => setModal({ ...modal, isOpen: false })}
        content={modal.content}
      />

      <div className="fw-bold mb-3">Total Products: {total}</div>

      <div className="row">
        {!products &&
          Array.from(Array(4).keys()).map((item: any) => (
            <div className="col-lg-3 mb-3" key={item}>
              <div className="card-product shadow">
                <div className="img-placeholder" />

                <div className="p-3">
                  <Placeholder animation="glow">
                    <div>
                      <Placeholder xs={6} />
                    </div>
                    <div>
                      <Placeholder xs={4} />
                    </div>
                    <div>
                      <Placeholder xs={8} />
                    </div>
                  </Placeholder>

                  <hr />

                  <Placeholder animation="glow">
                    <div>
                      <Placeholder xs={6} />
                    </div>
                    <div>
                      <Placeholder xs={4} />
                    </div>
                  </Placeholder>
                </div>
              </div>
            </div>
          ))}

        {products?.map((item: any) => (
          <div className="col-lg-3 mb-3" key={item.id}>
            <div className="card-product shadow">
              <img src={item.thumbnail} alt={item.title} />

              <div className="p-3">
                <strong>{item.title}</strong>

                <div>
                  <span className="color-gray">Brand:</span>{" "}
                  <strong>{item.brand}</strong>
                </div>

                <div>
                  <span className="color-gray">Category:</span>{" "}
                  <strong>{item.category}</strong>
                </div>

                <hr />

                <div>
                  <div className="color-gray">
                    Price:{" "}
                    <strong className="color-red">USD {item.price}</strong>
                  </div>
                  <div className="color-gray">
                    Stock: <strong className="color-black">{item.stock}</strong>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </>
  );
};

export default Home;
