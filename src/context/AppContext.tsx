import React, { createContext, useReducer } from "react";

export const AppContext = createContext<any>(null);

const profileInfo = JSON.parse(localStorage.getItem("profile") || "{}");

const initialState = {
  loading: true,
  toast: {
    show: false,
    type: "success",
    message: "",
    time: 0,
    centered: false,
  },
  profile: typeof window !== "undefined" && profileInfo ? profileInfo : null,
};

const reducer = (state: any, action: any) => {
  switch (action.type) {
    case "SET_LOADING":
      return {
        ...state,
        loading: action.payload,
      };
    case "SET_TOAST":
      return {
        ...state,
        toast: action.payload,
      };
    case "SET_PROFILE":
      localStorage.setItem("profile", JSON.stringify(action.payload));

      return {
        ...state,
        profile: action.payload,
      };
    case "LOGIN":
      localStorage.setItem("token", JSON.stringify(action.payload));

      return {
        ...state,
        loading: false,
      };
    case "LOGOUT":
      localStorage.removeItem("token");
      localStorage.removeItem("profile");
      return {
        ...state,
        profile: null,
      };
    default:
      throw new Error();
  }
};

export const AppContextProvider = (props: any) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={[state, dispatch]}>
      {props.children}
    </AppContext.Provider>
  );
};
